var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('css', function() {
    return gulp.src('./assets/scss/*.scss')
        .pipe( sourcemaps.init() )
        .pipe( sass({
            outputStyle: 'compressed',
        }).on('error', sass.logError) )
        .pipe( sourcemaps.write() )
        .pipe( gulp.dest('./css/') );
});

// Watch task
gulp.task('default',function() {
    gulp.watch('sass/**/*.scss',['css']);
});
